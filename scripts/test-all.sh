#!/bin/bash

fuzzer=${1:-afl}
RUNC=${2:-docker}

[ $(which $RUNC) ] || exit 1

if [ "$RUNC" = "singularity" ]; then
    simg=${HOME}/s_images/${fuzzer}.sif
    rm -f $simg
    singularity pull --force --name $simg shub://shub-fuzz/${fuzzer}
    logfile="testing-singularity-${fuzzer}.log"
else
    docker pull registry.gitlab.com/rode0day/fuzzer-testing/${fuzzer}_runner:16.04 || exit 1
    logfile="testing-docker-${fuzzer}.log"
fi
rm -f $logfile

read -r -d '' TARGETS << EOM
bloaty
curl
freetype2-2017
harfbuzz-1_3_2
jsoncpp
lcms-2017-03-21
libjpeg-turbo-07-2017
libpcap
libpng-1_2_56
libxml2-v2_9_2
libxslt
mbedtls
openssl
openthread-2019-12-23
php
proj4-2017-08-14
re2-2014-12-09
sqlite3
systemd
vorbis-2017-12-11
woff2-2016-05-06
zlib
EOM

SECONDS=0
if [ "$RUNC" = "singularity" ]; then
    echo "$TARGETS" | xargs -I{} -P 7 sh -c "nohup ./scripts/launch_job.sh ${fuzzer} '{}' 1 --test --no-sbatch > testing-singularity-${fuzzer}-{}.log"
else
    echo "$TARGETS" | xargs -I{} -P 7 sh -c "nohup ./scripts/launch.sh --test --fuzzer ${fuzzer} -N 1 '{}' > testing-docker-${fuzzer}-{}.log"
fi

logfile="testing-${RUNC}-${fuzzer}.log"
grep -h 'Finished' *-${RUNC}-${fuzzer}-*.log > $logfile
echo "[*] Finished in $SECONDS secs on $(date)" | tee -a $logfile
