#!/bin/bash

fuzzbench_root="/opt/fuzz/fuzzbench/benchmarks"

printf "challenges:\n"

i=1
for d in $(find $fuzzbench_root -name benchmark.yaml | sort ); do 
	bm=$(basename $(dirname $d)); 
	[[ $bm == ${bm#*_} ]] || continue
	p=${bm%%_*}
	ft="$(grep fuzz_target $d | cut -d' ' -f2)"
	printf "    ${bm}:\n"
	printf "        challenge_id: $i\n"
	printf '        architecture: "x86_64"\n'
	printf "        install_dir: \"${p//./_}\"\n"
	printf "        binary_path: \"${ft}\"\n\n"
	i="$(( $i + 1 ))"
done


for d in $(find $fuzzbench_root -name benchmark.yaml | sort ); do 
	bm=$(basename $(dirname $d)); 
	[[ $bm == ${bm#*_} ]] && continue
	p=${bm%%_*}
	ft="$(grep fuzz_target $d | cut -d' ' -f2)"
	printf "    ${bm}:\n"
	printf "        challenge_id: $i\n"
	printf '        architecture: "x86_64"\n'
	printf "        install_dir: \"${p}\"\n"
	printf "        binary_path: \"${ft}\"\n\n"

	i="$(( $i + 1 ))"
done
