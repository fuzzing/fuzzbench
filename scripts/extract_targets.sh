#!/bin/bash -ue


fuzzer=${1:-afl}
EROOT=${2:-/tmp/$fuzzer}
mkdir -p $EROOT
echo "[*] using $fuzzer, saving to $EROOT"


extract_fuzzbench() {
	local bm=$1
	local cname="${fuzzer}-${bm}"
	local savedir="$EROOT/$bm"
	mkdir -p "$savedir"
	local registry="gcr.io/fuzzbench/runners"
        # docker pull "${registry}/${fuzzer}/${bm}"
	docker create --name "$cname" "${registry}/${fuzzer}/${bm}" >/dev/null
        docker cp "$cname:/out/fuzz-target" $savedir/ >/dev/null
	docker cp "$cname:/out/seeds" $savedir/ >/dev/null
	if [[ $fuzzer = "qsym" ]]; then
		docker cp "$cname:/out/uninstrumented/fuzz-target" $savedir/fuzz-target-coverage
	fi
	docker rm $cname >/dev/null
}


extract_ossfuzz() {
	local bm=$1
	local project="$(grep project benchmarks/${bm}/oss-fuzz.yaml | cut -d ':' -f2 | tr -d ' ')"
	local target="$(grep target benchmarks/${bm}/oss-fuzz.yaml | cut -d ':' -f2 | tr -d ' ')"
	local savedir="$EROOT/$project"
	mkdir -p "$savedir"
	local cname="${fuzzer}-${project}"
	local registry="gcr.io/fuzzbench/oss-fuzz/runners"
	local dict_files="$(docker run --entrypoint find --name $cname ${registry}/${fuzzer}/${project} /out -type f -name '*.dict')"
        # docker pull "${registry}/${fuzzer}/${project}"
	docker cp "$cname:/out/$target" $savedir/ >/dev/null
	docker cp "$cname:/out/${target}_seed_corpus.zip" $savedir/ &>/dev/null || echo "[*] no seed corpus found"
	for df in $dict_files; do
		echo $df
		docker cp $cname:$df $savedir/ &>/dev/null || echo "[*] no dictionaries found"
	done
	if [[ $fuzzer = "qsym" ]]; then
		docker cp "$cname:/out/uninstrumented/${target}" $savedir/${target}-noins
	fi
	if [[ $bm == "systemd_fuzz-link-parser" ]]; then
		mkdir $savedir/src/shared/
		docker cp "$cname:/out/src/shared/libsystemd-shared-245.so" $savedir/src/shared/ >/dev/null
	fi
	docker rm $cname >/dev/null
}


for d in benchmarks/*/; do 
	bm="$(basename $d)"
	echo $bm
	if [ -e benchmarks/${bm}/build.sh ]; then
		extract_fuzzbench "$bm"
	elif [ -e benchmarks/${bm}/oss-fuzz.yaml ]; then
		extract_ossfuzz "$bm"
	else
		echo "[-] unknown registry for $bm"
	fi
done
