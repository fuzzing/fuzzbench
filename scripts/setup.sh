#!/bin/bash

create_afl_job_configs() {
	python3 scripts/create-configs.py -e example_job.json -y benchmarks/info.yaml -j AFL -p afl -c afl_job.json -M 0 -F >/dev/null
	python3 scripts/create-configs.py -e example_job.json -y benchmarks/info.yaml -j AFL -c job.json -M 0 -F >/dev/null
}

create_aflpp_job_configs() {
	python3 scripts/create-configs.py -e example_job.json -y benchmarks/info.yaml -j AFL++ -p aflpp -c aflpp_job.json \
                --cmplog -M 0 -F >/dev/null
}


fix_old_directories() {
	# Older versions had directories with '.'s in the name
	mv benchmarks/harfbuzz-1.3.2/* benchmarks/harfbuzz-1_3_2/
	mv benchmarks/libpng-1.2.56/* benchmarks/libpng-1_2_56/
	mv benchmarks/libxml2-v2.9.2/* benchmarks/libxml2-v2_9_2/
	rmdir benchmarks/harfbuzz-1.3.2
	rmdir benchmarks/libpng-1.2.56
	rmdir benchmarks/libxml2-v2.9.2
}


download_binaries() {
	local gitref=${GITREF:-v2021-06-01}
        local tmpfile="/tmp/afl_fuzzbench.zip"
        rm -f $tmpfile
	wget -O $tmpfile "https://gitlab.com/fuzzing/fuzzbench/-/jobs/artifacts/${gitref}/download?job=deploy:afl"
	python3 -m zipfile -e $tmpfile .
	chmod +x benchmarks/*/bin/*
	chmod +x benchmarks/*/noins/*
	chmod +x benchmarks/*/afl*/*
	chmod +x benchmarks/*/hf/*

}

download_extra_dicts() {
	wget -qO benchmarks/lcms-2017-03-21/icc.dict 'https://raw.githubusercontent.com/google/oss-fuzz/master/projects/lcms/icc.dict'
	wget -qO benchmarks/libjpeg-turbo-07-2017/jpeg.dict 'https://raw.githubusercontent.com/google/AFL/master/dictionaries/jpeg.dict'
	wget -qO benchmarks/re2-2014-12-09/regexp.dict 'https://raw.githubusercontent.com/google/AFL/master/dictionaries/regexp.dict'
	wget -qO benchmarks/libpng-1_2_56/png.dict 'https://raw.githubusercontent.com/google/AFL/master/dictionaries/png.dict'
	wget -qO benchmarks/libxml2-v2_9_2/xml.dict 'https://raw.githubusercontent.com/google/AFL/master/dictionaries/xml.dict'
	wget -qO benchmarks/libxslt/xpath.dict 'https://gitlab.gnome.org/GNOME/libxslt/-/raw/master/tests/fuzz/xpath.dict'
}

download_coverage_binaries() {
        local EXP="2021-06-02"
        local BM="bloaty"
	local tmpfile="/tmp/coverage_${BM}.tar.gz"
        wget -O $tmpfile "https://www.googleapis.com/download/storage/v1/b/fuzzbench-data/o/${EXP}/coverage-binaries/coverage-build-${BM}_fuzz_target.tar.gz?generation=1622492354136942&alt=media"
}

# rsync -am  --include='*noins'  --include='*/' --exclude='*' qsym/ /dev/shm/exp/fb_bins/benchmarks/

# for f in benchmarks/*/afl/bin/*; do mv -v $f ${f/bin\//}; done
# for f in benchmarks/*/aflpp/cmplog; do mv -v $f ${f/aflpp\/cmplog/}; done

create_afl_job_configs
create_aflpp_job_configs
download_binaries
download_extra_dicts
