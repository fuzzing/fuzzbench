

# Overview

| Library      | Fuzzer                   | Version | commit  | oss-f | FB-f  | bugs/source                   | Date | 
| -------      | ------                   | ------- | ------  | ----: | -----:| ----                          | ---- |
| Apache Arrow | parquet-arrow-fuzz       | v0.16.0 | 3bc01ec | 1     | 125   | [bugs][arrow-bugs]/ [src](https://github.com/apache/arrow) | 2020-02-13 |
| Harfbuzz     | hb-subset-fuzzer         | v2.7.0  | 48ad745 | 1     | 47    | [bugs][hb-bugs]/ [src](https://github.com/harfbuzz/harfbuzz) | 2020-07-29 |
| libhevc      | hevc_dec_fuzzer          |         | d28f221 | 4     | 2     | [bugs][hevc-bugs]/ [src](https://android.googlesource.com/platform/external/libhevc) | 2019-09-06 |
| matio        | matio_fuzzer             | v1.5.17 | 7a99584 | 16    | 1     | [bugs][matio-bugs]/ [src](https://github.com/tbeu/matio) | 2019-09-04 |
| openexr      | openexr_exrenvmap_fuzzer | v2.5.2  | 42d899a |       | 3     | [bugs][openexr-bugs]/ [src](https://github.com/AcademySoftwareFoundation/openexr) | 2020-07-29 |
| openh264     | decoder_fuzzer           | v2.0.0  | c185ac3 | 1     | 9     | [bugs][openh264-bugs]/ [src](https://github.com/cisco/openh264) | 2019-10-22 |
| php          | php-fuzz-execute         |         | 1902f73 | 7     | 27    | [bugs][php-e-bugs]/ [src](https://github.com/php/php-src) | 2020-08-29 |
| php          | php-fuzz-parser          |         | 8664ff7 |       | 16    | [bugs][php-p-bugs]/ [src](https://github.com/php/php-src) | 2020-07-25 |
| stb          | stbi_read_fuzzer         |         | f54acd4 | 12    | 13    | [bugs][stb-bugs]/ [src](https://github.com/nothings/stb) | 2020-02-05 |

**oss-f**: oss-fuzz found bugs (publicly released)

**FB-f**: fuzzbench found bugs (2020-12-19 report)

## Bug list

| bug id | type | fuzzer |
| ------ | ---- | ------ |
| [23600][oss-id-23600] | OOM     | parquet-arrow-fuzz |
| [25666][oss-id-25666] | Timeout | hb-subset-fuzzzer  |



[arrow-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-arrow
[parquet-arrow-fuzz]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=%22parquet-arrow-fuzz%22
[hb-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-harfbuzz
[hevc-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-libhevc
[matio-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-matio
[openexr-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-openexr
[openh264-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-openh264
[php-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-php
[php-e-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=%22php-fuzz-execute%22
[php-p-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=%22php-fuzz-parser%22
[stb-bugs]: https://bugs.chromium.org/p/oss-fuzz/issues/list?q=label:Proj-stb


[oss-id-23600]: https://bugs.chromium.org/p/oss-fuzz/issues/detail?id=23600
[oss-id-25666]: https://bugs.chromium.org/p/oss-fuzz/issues/detail?id=25666
