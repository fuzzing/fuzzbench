# fuzzbench-targets

Extracted fuzzing targets from Google Fuzzbench
https://google.github.io/fuzzbench/


## Quick start

1. Download fuzzing binaries and create fuzzing job configuration files.

```shell
$ ./scripts/setup.sh
```

2. [Optionally] edit job/fuzzer parameters file
```shell
$ vim benchmarks/curl/afl_job.json
```

3. Start fuzzing (launch in background with '&')
```shell
$ ./scripts/launch.sh --fuzzer afl --test curl
```
